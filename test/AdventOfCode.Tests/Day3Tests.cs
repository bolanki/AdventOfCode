﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day3Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var input = ReadAllLines("day3_part1_example.txt");
      var result = Day3.CountValidTriangles(input, true);
      Assert.Equal(0, result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllLines("day3.txt");
      var result = Day3.CountValidTriangles(input, true);
      Assert.Equal(983, result);
    }

    [Fact]
    public void Part2_Example()
    {
      var input = ReadAllLines("day3_part2_example.txt");
      var result = Day3.CountValidTriangles(input, false);
      Assert.Equal(6, result);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllLines("day3.txt");
      var result = Day3.CountValidTriangles(input, false);
      Assert.Equal(1836, result);
    }
  }
}
