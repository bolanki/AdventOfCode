﻿using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace AdventOfCode.Tests
{
  public class Day8Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var input = ReadAllLines("day8_example.txt");
      var result = Day8.Part1(input);
      Assert.Equal(6, result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllLines("day8.txt");
      var result = Day8.Part1(input);
      Assert.Equal(116, result);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllLines("day8.txt");
      var screenLines = new List<string>
      {
        "O  O OOO   OO    OO OOOO O    OOO   OO  OOOO OOOO ",
        "O  O O  O O  O    O O    O    O  O O  O O       O ",
        "O  O O  O O  O    O OOO  O    OOO  O    OOO    O  ",
        "O  O OOO  O  O    O O    O    O  O O    O     O   ",
        "O  O O    O  O O  O O    O    O  O O  O O    O    ",
        " OO  O     OO   OO  O    OOOO OOO   OO  OOOO OOOO "
      };

      var result = Day8.Part2(input, screenLines);
      Assert.Equal(true, result);
    }
  }
}
