﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day6Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var input = ReadAllLines("day6_example.txt");
      var result = Day6.Decryptsignal(input, false);
      Assert.Equal("easter", result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllLines("day6.txt");
      var result = Day6.Decryptsignal(input, false);
      Assert.Equal("ikerpcty", result);
    }

    [Fact]
    public void Part2_Example()
    {
      var input = ReadAllLines("day6_example.txt");
      var result = Day6.Decryptsignal(input, true);
      Assert.Equal("advent", result);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllLines("day6.txt");
      var result = Day6.Decryptsignal(input, true);
      Assert.Equal("uwpfaqrq", result);
    }
  }
}
