﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day4Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var input = ReadAllLines("day4_part1_example.txt");
      var result = Day4.GetSectorIdSum(input);
      Assert.Equal(1514, result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllLines("day4.txt");
      var result = Day4.GetSectorIdSum(input);
      Assert.Equal(137896, result);
    }

    [Fact]
    public void Part2_Example()
    {
      var input = ReadAllLines("day4_part2_example.txt");
      var room = Day4.FindRoom(input, "very encrypted name");
      Assert.Equal(343, room.SectorId);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllLines("day4.txt");
      var room = Day4.FindRoom(input, "northpole object storage");
      Assert.Equal(501, room.SectorId);
    }
  }
}
