﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day2Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var input = ReadAllLines("day2_example.txt");
      var result = Day2.GetCode(input, false);
      Assert.Equal("1985", result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllLines("day2.txt");
      var result = Day2.GetCode(input, false);
      Assert.Equal("33444", result);
    }

    [Fact]
    public void Part2_Example()
    {
      var input = ReadAllLines("day2_example.txt");
      var result = Day2.GetCode(input, true);
      Assert.Equal("5DB3", result);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllLines("day2.txt");
      var result = Day2.GetCode(input, true);
      Assert.Equal("446A6", result);
    }
  }
}
