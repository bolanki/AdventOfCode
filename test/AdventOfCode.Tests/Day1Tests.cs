﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day1Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var input = ReadAllText("day1_part1_example.txt");
      var result = Day1.Part1(input);
      Assert.Equal(12, result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllText("day1.txt");
      var result = Day1.Part1(input);
      Assert.Equal(181, result);
    }

    [Fact]
    public void Part2_Example()
    {
      var input = ReadAllText("day1_part2_example.txt");
      var result = Day1.Part2(input);
      Assert.Equal(4, result);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllText("day1.txt");
      var result = Day1.Part2(input);
      Assert.Equal(140, result);
    }
  }
}
