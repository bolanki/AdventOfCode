﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day7Tests : BaseTest
  {
    [Fact]
    public void Part1_Example1()
    {
      var result = Day7.CountTlsSupport(new [] { "abba[mnop]qrst" });
      Assert.Equal(1, result);
    }

    [Fact]
    public void Part1_Example2()
    {
      var result = Day7.CountTlsSupport(new[] { "abcd[bddb]xyyx" });
      Assert.Equal(0, result);
    }

    [Fact]
    public void Part1_Example3()
    {
      var result = Day7.CountTlsSupport(new[] { "aaaa[qwer]tyui" });
      Assert.Equal(0, result);
    }

    [Fact]
    public void Part1_Example4()
    {
      var result = Day7.CountTlsSupport(new[] { "ioxxoj[asdfgh]zxcvbn" });
      Assert.Equal(1, result);
    }

    [Fact]
    public void Part1()
    {
      var input = ReadAllLines("day7.txt");
      var result = Day7.CountTlsSupport(input);
      Assert.Equal(110, result);
    }

    [Fact]
    public void Part2_Example1()
    {
      var result = Day7.CountSslSupport(new[] { "aba[bab]xyz" });
      Assert.Equal(1, result);
    }

    [Fact]
    public void Part2_Example2()
    {
      var result = Day7.CountSslSupport(new[] { "xyx[xyx]xyx" });
      Assert.Equal(0, result);
    }

    [Fact]
    public void Part2_Example3()
    {
      var result = Day7.CountSslSupport(new[] { "aaa[kek]eke" });
      Assert.Equal(1, result);
    }

    [Fact]
    public void Part2_Example4()
    {
      var result = Day7.CountSslSupport(new[] { "zazbz[bzb]cdb" });
      Assert.Equal(1, result);
    }

    [Fact]
    public void Part2()
    {
      var input = ReadAllLines("day7.txt");
      var result = Day7.CountSslSupport(input);
      Assert.Equal(242, result);
    }
  }
}
