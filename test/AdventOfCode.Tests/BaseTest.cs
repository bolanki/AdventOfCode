﻿using System.IO;

namespace AdventOfCode.Tests
{
  public class BaseTest
  {
    public string ReadAllText(string filename)
    {
      return File.ReadAllText(Path.Combine(".", "Input", filename));
    }

    public string[] ReadAllLines(string filename)
    {
      return File.ReadAllLines(Path.Combine(".", "Input", filename));
    }
  }
}
