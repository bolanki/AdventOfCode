﻿using Xunit;

namespace AdventOfCode.Tests
{
  public class Day5Tests : BaseTest
  {
    [Fact]
    public void Part1_Example()
    {
      var result = Day5.GetPassword("abc", false);
      Assert.Equal("18f47a30", result);
    }

    [Fact]
    public void Part1()
    {
      var result = Day5.GetPassword("abbhdwsy", false);
      Assert.Equal("801b56a7", result);
    }

    [Fact]
    public void Part2_Example()
    {
      var result = Day5.GetPassword("abc", true);
      Assert.Equal("05ace8e3", result);
    }

    [Fact]
    public void Part2()
    {
      var result = Day5.GetPassword("abbhdwsy", true);
      Assert.Equal("424a0197", result);
    }
  }
}
