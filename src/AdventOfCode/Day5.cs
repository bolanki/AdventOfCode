﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode
{
  public class Day5
  {
    public static string GetPassword(string doorId, bool isIndexedPassword)
    {
      var password = new List<char?>();
      for (var i = 0; i < 8; i++)
      {
        password.Add(null);
      }
      var counter = 0;
      var charactersAdded = 0;


      while (charactersAdded < 8)
      {
        var currentHash = GetMd5Hash($"{doorId}{counter}");
        counter++;

        if (!currentHash.StartsWith("00000")) continue;

        if (isIndexedPassword)
        {
          int index;
          if (!int.TryParse(currentHash[5].ToString(), out index)) continue;
          if (index > 7) continue;
          if (password[index] != null) continue;
          password[index] = currentHash[6];
          charactersAdded++;
        }
        else
        {
          password[charactersAdded] = currentHash[5];
          charactersAdded++;
        }
      }
      return new string(password.Select(x => x.Value).ToArray()).ToLower();
    }

    private static string GetMd5Hash(string input)
    {
      var encodedPassword = new UTF8Encoding().GetBytes(input);
      var hash = MD5.Create().ComputeHash(encodedPassword);
      return BitConverter.ToString(hash).Replace("-", string.Empty);
    }
  }
}
