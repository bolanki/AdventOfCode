﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode
{
  public class Day7
  {
    public static int CountTlsSupport(string[] input)
    {
      var validIp7Count = 0;

      foreach (var s in input)
      {
        var matches = Regex.Matches(s, @"\[(.*?)\]");
        var replacedString = s;
        var hypernetSequences = new List<string>();
        foreach (Match m in matches)
        {
          hypernetSequences.Add(m.Groups[1].Value);
          replacedString = replacedString.Replace(m.Groups[0].Value, "|");
        }

        if (IsValidAbbaIp(hypernetSequences.ToArray()))
        {
          continue;
        }

        var validPairings = replacedString.Split('|');
        if (IsValidAbbaIp(validPairings))
        {
          validIp7Count++;
        }
      }

      return validIp7Count;
    }

    public static int CountSslSupport(string[] input)
    {
      var validSslCount = 0;

      foreach (var s in input)
      {
        var matches = Regex.Matches(s, @"\[(.*?)\]");
        var replacedString = s;
        var hypernetSequences = new List<string>();
        foreach (Match m in matches)
        {
          hypernetSequences.Add(m.Groups[1].Value);
          replacedString = replacedString.Replace(m.Groups[0].Value, "|");
        }

        var validPairings = replacedString.Split('|');
        var abas = GetAllAba(validPairings);
        if (IsValidBabForAbas(hypernetSequences, abas))
        {
          validSslCount++;
        }
      }

      return validSslCount;
    }

    private static List<string> GetAllAba(string[] supernets)
    {
      var validAbas = new List<string>();
      foreach (var supernet in supernets)
      {
        var charArray = supernet.ToCharArray();
        for (var i = 0; i < charArray.Length - 2; i++)
        {
          if ((charArray[i] != charArray[i + 1]) && (charArray[i] == charArray[i + 2]))
          {
            validAbas.Add($"{charArray[i]}{charArray[i+1]}{charArray[i+2]}");
          }
        }
      }
      return validAbas;
    }

    private static bool IsValidBabForAbas(List<string> hypernets, List<string> abas)
    {
      foreach (var hypernet in hypernets)
      {
        var charArray = hypernet.ToCharArray();
        for (var i = 0; i < charArray.Length - 2; i++)
        {
          if ((charArray[i] != charArray[i + 1]) && (charArray[i] == charArray[i + 2]))
          {
            if (abas.Contains($"{charArray[i + 1]}{charArray[i]}{charArray[i + 1]}"))
            {
              return true;
            }
          }
        }
      }
      return false;
    }

    private static bool IsValidAbbaIp(string[] ips)
    {
      return ips.Any(IsValidAbba);
    }

    private static bool IsValidAbba(string abba)
    {
      if (abba.Length < 4)
      {
        return false;
      }

      var charArray = abba.ToCharArray();
      for (var i = 0; i < charArray.Length - 3; i++)
      {
        if (charArray[i] == charArray[i + 1]) continue;
        if (charArray[i] == charArray[i + 3]  && charArray[i + 1] == charArray[i + 2])
        {
          return true;
        }
      }
      return false;
    }
  }
}
