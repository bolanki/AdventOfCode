﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
  public class Day6
  {
    public static string Decryptsignal(string[] input, bool userReverseRepetition)
    {
      var rowCount = input.Length;
      var columnCount = input[0].Length;
      var signalCharacters = new List<List<char>>();

      for (var i = 0; i < rowCount; i++)
      {
        var characterList = new List<char>();
        for (var j = 0; j < columnCount; j++)
        {
          characterList.Add(input[i][j]);
        }
        signalCharacters.Add(characterList);
      }

      var decryptedSignal = string.Empty;
      for (var i = 0; i < columnCount; i++)
      {
        var groupBy = signalCharacters.Select(x => x[i]).GroupBy(x => x);
        var orderedCollection = userReverseRepetition ? groupBy.OrderBy(x => x.Count()) : groupBy.OrderByDescending(x => x.Count());
        decryptedSignal += orderedCollection.FirstOrDefault().Key;
      }

      return decryptedSignal;
    }
  }
}
