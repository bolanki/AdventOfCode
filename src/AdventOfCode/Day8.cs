﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace AdventOfCode
{
  public class Day8
  {
    private const int ScreenRows = 6;
    private const int ScreenColumns = 50;
    private static readonly bool[,] Screen = new bool[ScreenRows, ScreenColumns];

    public static int Part1(string[] input)
    {
      Setup(input);
      return CountOnLights();
    }

    public static bool Part2(string[] input, List<string> expectedResult)
    {
      Setup(input);
      var result = new List<string>();
      for (var i = 0; i < ScreenRows; i++)
      {
        var rowValue = string.Empty;
        for (var j = 0; j < ScreenColumns; j++)
        {
          rowValue = $"{rowValue}{(Screen[i, j] ? "O" : " ")}";
        }
        result.Add(rowValue);
      }
      for (var i = 0; i < ScreenRows; i++)
      {
        if (expectedResult[i] != result[i])
        {
          return false;
        }
      }
      return true;
    }

    private static void Setup(IEnumerable<string> input)
    {
      foreach (var s in input)
      {
        if (s.StartsWith("rect"))
        {
          SetRectangle(s);
        }
        else
        {
          var isRow = false;
          string equation;
          if (s.StartsWith("rotate column"))
          {
            equation = s.Replace("rotate column x=", "");
          }
          else
          {
            isRow = true;
            equation = s.Replace("rotate row y=", "");
          }
          var equationPair = equation.Split(new[] { " by " }, StringSplitOptions.None);
          var targetIndex = int.Parse(equationPair[0]);
          var shiftCount = int.Parse(equationPair[1]);
          Rotate(targetIndex, shiftCount, isRow ?  ScreenColumns : ScreenRows, isRow);
        }
      }
    }

    private static void SetRectangle(string rectangle)
    {
      var equation = rectangle.Split(' ')[1];
      var equationPair = equation.Split('x');
      var x = int.Parse(equationPair[0]);
      var y = int.Parse(equationPair[1]);

      for (var i = 0; i < y; i++)
      {
        for (var j = 0; j < x; j++)
        {
          Screen[i, j] = true;
        }
      }
    }

    private static void Rotate(int targetIndex, int shiftCount, int limit, bool isRow)
    {

      for (var shiftCounter = 0; shiftCounter < shiftCount; shiftCounter++)
      {
        var lastValue = isRow ? Screen[targetIndex, limit - 1] : Screen[limit - 1, targetIndex];
        var currentIndex = 0;
        for (var itemCounter = 0; itemCounter < limit; itemCounter++)
        {
          bool currentValue;
          if (isRow)
          {
            currentValue = Screen[targetIndex, currentIndex];
            Screen[targetIndex, currentIndex] = lastValue;
          }
          else
          {
            currentValue = Screen[currentIndex, targetIndex];
            Screen[currentIndex, targetIndex] = lastValue;
          }
          lastValue = currentValue;
          currentIndex++;
          if (currentIndex == limit)
          {
            currentIndex = 0;
          }
        }
      }
    }

    private static int CountOnLights()
    {
      var onLights = 0;
      for (var i = 0; i < ScreenRows; i++)
      {
        for (var j = 0; j < ScreenColumns; j++)
        {
          if (Screen[i, j])
          {
            onLights++;
          }
        }
      }
      return onLights;
    }
  }
}
