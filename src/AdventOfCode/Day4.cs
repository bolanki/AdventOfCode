﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
  public class Day4
  {
    public static int GetSectorIdSum(string[] rooms)
    {
      var roomList = rooms.Select(room => new Room(room)).ToList();
      return roomList.Where(x => x.Checksum == x.GeneratedChecksum).Sum(x => x.SectorId);
    }

    public static Room FindRoom(string[] rooms, string roomName)
    {
      var roomList = rooms.Select(room => new Room(room)).ToList();
      return roomList.First(x => x.ShiftCipher() == roomName);
    }

    public class Room
    {
      private const int LetterAValue = 'a';
      private const int LetterZValue = 'z';
      private string Code { get; }
      public int SectorId { get; }
      public string Checksum { get; }

      public Room(string input)
      {
        var seperatorIndex = input.LastIndexOf('-');
        Code = input.Substring(0, seperatorIndex);
        var sectorIdAndChecksum = input.Substring(seperatorIndex + 1, input.Length - 1 - seperatorIndex).Split('[');
        SectorId = int.Parse(sectorIdAndChecksum[0]);
        Checksum = sectorIdAndChecksum[1].Remove(sectorIdAndChecksum[1].Length - 1);
      }

      public string GeneratedChecksum
      {
        get
        {
          var withoutDashes = Code.Replace("-", "");
          var letterGroupings = withoutDashes.GroupBy(x => x).OrderByDescending(x => x.Count()).ThenBy(x => x.Key).Take(5);
          return new string(letterGroupings.Select(x => x.Key).ToArray());
        }
      }

      public string ShiftCipher()
      {
        var codeCharacters = new List<char>(Code);
        for (var i = 0; i < SectorId; i++)
        {
          for (var j = 0; j < codeCharacters.Count; j++)
          {
            switch (codeCharacters[j])
            {
              case ' ':
                continue;
              case '-':
                codeCharacters[j] = ' ';
                break;
              default:
                var newCharValue = codeCharacters[j] + 1;
                newCharValue = newCharValue > LetterZValue ? LetterAValue : newCharValue;
                codeCharacters[j] = Convert.ToChar(newCharValue);
                break;
            }
          }
        }

        return new string(codeCharacters.ToArray());
      }
    }
  }
}
