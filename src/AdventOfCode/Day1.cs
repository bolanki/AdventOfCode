﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
  public class Day1
  {
    private static string[] _movements;
    private static Direction _currentdirection = Direction.North;
    private static readonly List<Tuple<int, int>> VisitedBlockLocations = new List<Tuple<int, int>>();
    private static Tuple<int, int> _currentBlockLocation;
    private static Tuple<int, int> _firstPathCrossing;

    private enum Direction
    {
      North,
      East,
      South,
      West
    }

    private static bool IsHorizontalMovement => _currentdirection == Direction.West || _currentdirection == Direction.East;

    private static int GetTotalBlocksMoved(int blocks)
    {
      return blocks < 0 ? blocks * -1 : blocks;
    }

    private static int GetModifier(bool leftTurn)
    {
      return _currentdirection == Direction.North || _currentdirection == Direction.West ? (leftTurn ? -1 : 1) : (leftTurn ? 1 : -1);
    }

    private static void SetNewDirection(bool turnLeft)
    {
      switch (_currentdirection)
      {
        case Direction.North:
          _currentdirection = turnLeft ? Direction.West : Direction.East;
          break;
        case Direction.West:
          _currentdirection = turnLeft ? Direction.South : Direction.North;
          break;
        case Direction.South:
          _currentdirection = turnLeft ? Direction.East : Direction.West;
          break;
        case Direction.East:
          _currentdirection = turnLeft ? Direction.North : Direction.South;
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }

    private static void Move(string movement)
    {
      var turnLeft = movement[0] == 'L';
      var blocks = int.Parse(movement.Substring(1, movement.Length - 1));

      var modifier = GetModifier(turnLeft);

      for (var i = 1; i <= blocks; i++)
      {
        var directionModifier = 1 * modifier;
        var newLocation = IsHorizontalMovement 
          ? new Tuple<int, int>(_currentBlockLocation.Item1, _currentBlockLocation.Item2 + directionModifier) 
          : new Tuple<int, int>(_currentBlockLocation.Item1 + directionModifier, _currentBlockLocation.Item2);
          
        if (_firstPathCrossing == null && VisitedBlockLocations.Contains(newLocation))
        {
          _firstPathCrossing = newLocation;
        }
        VisitedBlockLocations.Add(newLocation);
        _currentBlockLocation = newLocation;
      }

      SetNewDirection(turnLeft);
    }

    private static void Setup(string input)
    {
      input = input.Replace(" ", "");
      _movements = input.Split(',');
      _currentBlockLocation = new Tuple<int, int>(0, 0);
      VisitedBlockLocations.Add(_currentBlockLocation);
    }

    public static int Part1(string input)
    {
      Setup(input);

      foreach (var movement in _movements)
      {
        Move(movement);
      }
      var lastVisitedLocation = VisitedBlockLocations.Last();
      return GetTotalBlocksMoved(lastVisitedLocation.Item1) + GetTotalBlocksMoved(lastVisitedLocation.Item2);
    }

    public static int Part2(string input)
    {
      Setup(input);

      foreach (var movement in _movements)
      {
        Move(movement);
        if (_firstPathCrossing != null)
        {
          return GetTotalBlocksMoved(_firstPathCrossing.Item1) + GetTotalBlocksMoved(_firstPathCrossing.Item2);
        }
      }
      throw new Exception("No crossing was made... maybe less eggnog next time?");
    }
  }
}
