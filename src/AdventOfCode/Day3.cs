﻿using System;
using System.Linq;

namespace AdventOfCode
{
  public class Day3
  {
    private static bool IsTriangle(int side1, int side2, int side3)
    {
      return (side1 + side2 > side3) && (side2 + side3 > side1) && (side1 + side3 > side2);
    }

    private static int[] GetTriangleSides(string triangle)
    {
      return triangle.Split(new[] { "  " }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
    }

    public static int CountValidTriangles(string[] triangles, bool isHorizontalProcessing)
    {
      if (isHorizontalProcessing)
      {
        return triangles.Select(GetTriangleSides)
          .Select(sides => IsTriangle(sides[0], sides[1], sides[2]) ? 1 : 0)
          .Sum();
      }

      var validTriangles = 0;
      for (var i = 0; i < triangles.Length-2; i+=3)
      {
        var row = GetTriangleSides(triangles[i]);
        var row2 = GetTriangleSides(triangles[i+1]);
        var row3 = GetTriangleSides(triangles[i+2]);

        for (var j = 0; j < 3; j++)
        {
          validTriangles += IsTriangle(row[j], row2[j], row3[j]) ? 1 : 0;
        }
      }
      return validTriangles;
      
    }
  }
}
