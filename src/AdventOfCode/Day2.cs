﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
  public class Day2
  {
    private static string[,] _keyPad;
    private static Tuple<int, int> _currentKeyPadLocation;

    private static string GetHexNumber(int number)
    {
      return number.ToString("X");
    }

    private static void Setup(bool advancedKeyPad)
    {
      if (advancedKeyPad)
      {
        _keyPad = new string[5, 5];
        _currentKeyPadLocation = new Tuple<int, int>(2, 0);

        _keyPad[0, 2] = GetHexNumber(1);

        _keyPad[1, 1] = GetHexNumber(2);
        _keyPad[1, 2] = GetHexNumber(3);
        _keyPad[1, 3] = GetHexNumber(4);

        _keyPad[2, 0] = GetHexNumber(5);
        _keyPad[2, 1] = GetHexNumber(6);
        _keyPad[2, 2] = GetHexNumber(7);
        _keyPad[2, 3] = GetHexNumber(8);
        _keyPad[2, 4] = GetHexNumber(9);

        _keyPad[3, 1] = GetHexNumber(10);
        _keyPad[3, 2] = GetHexNumber(11);
        _keyPad[3, 3] = GetHexNumber(12);

        _keyPad[4, 2] = GetHexNumber(13);

      }
      else
      {
        _keyPad = new string[3, 3];
        _currentKeyPadLocation = new Tuple<int, int>(1, 1);

        var counter = 0;
        for (var row = 0; row < 3; row++)
        {
          for (var column = 0; column < 3; column++)
          {
            counter++;
            _keyPad[row, column] = counter.ToString();
          }
        }
      }
    }

    private static string GetCodeDigit(string movement)
    {
      var movements = movement.ToCharArray();
      foreach (var direction in movements)
      {
        var gridLength = _keyPad.GetLength(0);
        Tuple<int, int> newLocation = null;
        switch (direction)
        {
          case 'U':
            if (_currentKeyPadLocation.Item1 != 0)
            {
              newLocation = new Tuple<int, int>(_currentKeyPadLocation.Item1 - 1, _currentKeyPadLocation.Item2);
            }
            break;
          case 'D':
            if (_currentKeyPadLocation.Item1 != gridLength - 1)
            {
              newLocation = new Tuple<int, int>(_currentKeyPadLocation.Item1 + 1, _currentKeyPadLocation.Item2);
            }
            break;
          case 'L':
            if (_currentKeyPadLocation.Item2 != 0)
            {
              newLocation = new Tuple<int, int>(_currentKeyPadLocation.Item1, _currentKeyPadLocation.Item2 - 1);
            }
            break;
          case 'R':
            if (_currentKeyPadLocation.Item2 != gridLength - 1)
            {
              newLocation = new Tuple<int, int>(_currentKeyPadLocation.Item1, _currentKeyPadLocation.Item2 + 1);
            }
            break;
        }
        if (newLocation != null && _keyPad[newLocation.Item1, newLocation.Item2] != null)
        {
          _currentKeyPadLocation = newLocation;
        }
      }

      var currentLocationValue = _keyPad[_currentKeyPadLocation.Item1, _currentKeyPadLocation.Item2];
      return currentLocationValue ?? string.Empty;
    }

    public static string GetCode(string[] input, bool userAdvancedKeypad)
    {
      Setup(userAdvancedKeypad);
      var strCode = input.Aggregate(string.Empty, (current, s) => $"{current}{GetCodeDigit(s)}");
      return strCode;
    }
  }
}
